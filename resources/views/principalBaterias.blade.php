@extends('templates.base')

@section('conteudo')
<main>
    <h1>Turma: 2D1 - Grupo 1</h1>
    <h2>Participantes</h2>
    <hr>
    <table class="table table-striped table-bordered">
    <tr>
        <td>Matricula    </td>
        <td>Nome</td>
        <td>Função</td>
    </tr>
    <tr>
        <td>0072524</td>
        <td>Rayca Lopes</td>
        <td>Gerente, Medições</td>
    </tr>
    <tr>
        <td>0072533</td>
        <td>Ana Luiza</td>
        <td>Desenvolvedor, Medições</td>
    </tr>
    <tr>
        <td>0073312</td>
        <td>Luana Lima</td>
        <td>Desenvolvedor, Medições</td>
    </tr>
    <tr>
        <td>0072540</td>
        <td>Pedro Ferreira</td>
        <td>Desenvolvedor HTML</td>
    </tr>
    <tr>
        <td>0072532</td>
        <td>Tiago Lopes</td>
        <td>Desenvolvedor HTML</td>
    </tr>
   </table>
   <img src="imgs/WhatsApp Image 2023-07-19 at 10.43.32.jpeg" width="250px">
  </main>
 
  @endsection

  @section('rodape')
   <h4>Rodapé da página principal</h4>
  @endsection